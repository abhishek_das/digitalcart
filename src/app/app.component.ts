import {Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private _http: HttpClient){}

  iphoneCount: number = 1;
  s8Count: number = 1;
  order: any;
  orderNumberCreated: string;
  orderStatus: any;
  orderCreated: any;

  ngOnInit(): void {

    console.log('AppComponent is loaded ...');

    this._http.get('assets/order.json').subscribe(
      (data) => {
        this.order = data;
        console.log(this.order);
      }
    );
  }

  updateIPhoneCount(type){
    if(type === 'add'){
      this.iphoneCount++;
    }else{
      if(this.iphoneCount > 1){
        this.iphoneCount--;
      }
    }
  }

  updateS8Count(type){
    if(type === 'add'){
      this.s8Count++;
    }else{
      if(this.s8Count > 1){
        this.s8Count--;
      }
    }
  }

  placeOrder(){
     this._http.post('assets/order-submitted.json', this.order).subscribe(
       (data) => {
	 this.orderCreated = data;
         console.log(this.orderNumberCreated);
       }
     );
   }


  showTimeline(){
    this._http.get('assets/order-timeline.json' + this.orderCreated.id).subscribe(
      (data) => {
        this.orderStatus = data;
        console.log(this.orderStatus);
      }
    );
  }

}
