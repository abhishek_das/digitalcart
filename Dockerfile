FROM node:8

ENV HOME=/usr/src/app
RUN mkdir -p $HOME
WORKDIR $HOME

USER root

COPY . /usr/src/app

# Install dependecies
RUN npm install

EXPOSE 4200

# Serve the app
CMD ["npm", "start"]
